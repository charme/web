<?php

namespace App\Http\Middleware;

use Closure;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use charmeapi;
class TrackUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if ($request->Session()->get('Customer')) {
            return redirect('services');
        }
        if ($request->Session()->get('Professional')) {
            return redirect('professional/services');            
        }
        return $next($request);
    }
}

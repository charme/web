<?php

namespace App\Http\Middleware;

use Closure;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use charmeapi;

class SupplierSigninMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (!$request->Session()->get('supplier_id') || !$request->Session()->get('supplier_token')) {
            return redirect('/signin/professional')->with('signin_error','Kindly login');
        }
        $user_id=Session::get('supplier_id');            
            $user_details = Controller::charmeapi()->request('GET', 'suppliers/'.$user_id)->getBody();
            $user_details = json_decode($user_details,false);
            $user_details= $user_details->data->Supplier;
            $request->session()->put('Professional',$user_details);

        return $next($request);
    }
}

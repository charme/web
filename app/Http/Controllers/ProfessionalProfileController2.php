<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class ProfessionalProfileController extends Controller
{
    public function view_profile(){
    	$data = array();
    	$supplier_id=Session::get('supplier_id');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}")->getBody();
    	$response = (string) $response;
    	$response = json_decode($response,true);
    	$data = array_add($data,'response',$response);
    	return view('professional_modules.profile')->with('data', $data);
    }

    public function edit_profile(){
    	/*Get existing User Data and prepopulate the form*/
    	
    	$data = array();
    	$supplier_id=Session::get('supplier_id');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}")->getBody();
    	$response = (string) $response;
    	$response = json_decode($response,true);
    	$data = array_add($data,'response',$response);

    	return view('professional_modules.edit_profile')->with('data', $data);
    }

 	public function update_profile(request $request){ 		
 		$updated_details=$request->all();
 		//return $updated_details;
 		$data = array();
    	$supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');

 		$response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$updated_details])->getBody();
 		$response = $this->ArrayResponse($response);
    	$data = array_add($data,'response',$response);
 		//return $data;
 		if ($data['response']['status']==='ok') {
 			
    	return redirect('professional/profile')->with('profile_updated','Profile Successfully Updated');
 		}
        else return redirect('professional/profile/edit')->with('Update_failed','Profile Update Failed, Try Again');
    }   

    public function update_description(request $request){
        //$updated_details=$request->description;
        $supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');
        $update_description = array('description' => $request->description, );
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$update_description])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
    }

    public function location(request $request){
        return view('professional_modules.profile_location');
    }

    public function save_location(request $request){
        /*save latlong*/
        $latlong = $request->location;
        if (!empty($request->address_details)) {
        $address_details=$request->address.'('.$request->address_details.')';
        }
        else $address_details=$request->address;
        $latlong = trim($latlong,'(');
        $latlong = trim($latlong,')');
        $location_split= explode(",", $latlong);
        $lat=(float) trim($location_split[0]);
        $long=(float) $location_split[1];

        $save_address = array('latitude' => $lat,'longitude'=>$long, 'address_details'=> $address_details);
        $save_address=json_encode($save_address);
        return $save_address;
        $supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');
        $location = array('location' => $save_address, );
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$location])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
    }
    public function update_dp(request $request){ 
        /*$dd= $request->file('pics');
         //print_r($dd);
        $tmp_name= $request->file('pics')->getRealPath();
        $size= $request->file('pics')->getClientSize();


        $data = fopen($dd, 'rb');
        $size = filesize($dd);
        $contents = fread($data, $size);
        fclose($data);
        return $contents;
        $encoded = base64_encode($contents);

        return $encoded;*/

        $dd= $request->file('pics');
        $token=session('supplier_token');
        $id=session('supplier_id');
        //return $token;
         $upload = array('id' => session('supplier_id'), 
            'token'=> session('supplier_token'),
            'pics'=>$request->file('pics')
            );
         $pics = array('pics' => $request->file('pics'));
         $upload=json_encode($upload);
         //return $upload;
        $uri=getenv("CHARME_API");
        //return $url;
         $response = \Httpful\Request::post($uri."suppliers/$id/profile-pics?token=$token")->expectsJson()
         ->addHeader('Accept', "application/json")
         ->addHeader('Content-Type', "multipart/form-data");
         $response=$response->attach($pics);
         $response=$response->send();
        $response= json_decode((string) $response,true);
        return $response;

        /*$name=  $request->file('pics')->getClientOriginalName(); 
        $type= $request->file('pics')->getMimeType();
        $error= $request->file('pics')->getError();
        //$RealPath= 'C:/Users/KunleOdusan/Pictures/charme3ww.png';
        $tmp_name= $RealPath;*/
        
        /*$image= $request->file('pics')->getRealPath();
        $image= addslashes($image);
        $name= addslashes($name);
        $image= file_get_contents($image);
        $image= base64_encode($image);*/
        //$tmp_name=$image;

       /* $pics = array('pics' =>[
                'name'=> $name,
                'type' => $type,
                'tmp_name'=> $tmp_name,
                'error' => $error,
                'size' => $size
            ] , );
        print_r($pics);
        
        $fields = array("name" => "pics");
        //return $tmp_name;
        $binary= fopen($tmp_name, 'r');
        //return $binary;
        $fields["pics"] = fopen($tmp_name, 'r');

        //return $fields;
        $data = array();*/

    }
}

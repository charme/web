<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Carbon\Carbon;
class ProfessionalAppointmentController extends Controller
{
	public function show_appointments(){
		$id=session('supplier_id');
		$token=session('supplier_token');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$id}/appointments?token={$token}")->getBody();
    	$response= $this->ArrayResponse($response);
    	$past = $this->charmeapi()->request('GET', "suppliers/{$id}/appointments?token={$token}&type=past")->getBody();
    	$past= $this->ArrayResponse($past);
    	//return $past;
    	$response= array_add($response,'past',$past);
    	//return $response;            	
        return view('professional_modules.appointments',['data'=>$response]);

	}

    public function start_appointment(request $request){
        //return $request->all();
        $time= Carbon::now()->toDateTimeString();
        $appointment_id=$request->id;
        $token=session('supplier_token');
        $start = array('start_time' => $time,'token'=>$token);
        //return $start;
        $response = $this->charmeapi()->request('POST', "appointments/{$appointment_id}/start",['form_params' => $start])->getBody();
        $response= $this->ArrayResponse($response);
        return $response;
    }

    public function get_details ($id) {
                $response = $this->charmeapi()->request('GET', "appointments/{$id}")->getBody();
                $response= $this->ArrayResponse($response);
                //return $response;
                return view('professional_modules.appointment_details',['appointment'=>$response['data']]);
    }

    public function end_service ($id) {
                return view('professional_modules.service_completed',['id'=>$id]);
    }


    public function send_end(request $request) {
        $id=session('supplier_id');
        $comment=$request->comment;
        //return $comment;
        if ($comment=='') {
            $comment='ended';
        }
        $token=session('supplier_token');
        $appointment_id=$request->id;
        $end = array('id' => $request->id, 
            'comment'=> $comment,
            'rating'=>$request->rating,
            'token'=>$token,
            'end_time'=> Carbon::now()->toDateTimeString(),
            );
        //return $end;
        $response = $this->charmeapi()->request('POST', "appointments/{$appointment_id}/end?token={$token}",['form_params' => $end])->getBody();
        $response= $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return redirect('professional/appointments')->with('status', 'Appointment ended, Good Job');
        }
        if ($response['status']=='error') {
            return redirect('professional/appointments')->with('error', $response['error']['msg']);        
        }
        else return redirect('professional/appointments');
    }

	public function cancel_appointment(request $request){
		$id=$request->id;
		//return $id;
		$token=session('supplier_token');
        $cancel = array('token' => $token);
    	$response = $this->charmeapi()->request('POST', "appointments/{$id}/cancel?token={$token}",['form_params' => $cancel])->getBody();
        $response= $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		return redirect('professional/appointments')->with('status','Appointment has been cancelled');
    	}
    	if ($response['status']=='error') {
    		return redirect('professional/appointments')->with('error','Appointment was not cancel');
    	}
	}

    public function show_chat(request $request){
        $appointment_id = $request->id;
        /*Save the appointment_id so it can be used to post messages*/
        session::put('appointment_id',$appointment_id);
        //return $appointment_id;
        $id=session('supplier_id');
        $token=session('supplier_token');

        $response = $this->charmeapi()->request('GET', "appointments/{$appointment_id}/conversations?token={$token}")->getBody();
        $response= $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return view('professional_modules.chat',['messages'=>$response['data']]);            
        }
        elseif ($response['status']=='error') {
            return redirect('professional/appointments')->with('error',$response['error']['msg']);
        }
        else return redirect('professional/apoointments');
    }

    public function show_location(request $request){
        $appointment_id = $request->id;
        /*Save the appointment_id so it can be used to post messages*/
        session::put('appointment_id',$appointment_id);
        //return $appointment_id;
        $id=session('supplier_id');
        $token=session('supplier_token');

        $response = $this->charmeapi()->request('GET', "appointments/{$appointment_id}?token={$token}")->getBody();
        $response= $this->ArrayResponse($response);
        //return $response['data'];
        if ($response['status']=='ok') {
            return view('professional_modules.client_location',['data'=>$response['data']]);
            
        }
        elseif ($response['status']=='error') {
            return redirect('/appointments')->with('error',$response['error']['msg']);
        }
        else return redirect('/apoointments');
    }

    public function send_chat(request $request){
        /*Get appointment_id and message*/
        $id=session('appointment_id');
        $message= $request->message;
        $token=session('supplier_token');
        $sendchat = array('message' => $message,'token'=>$token);
        $response = $this->charmeapi()->request('POST', "appointments/{$id}/conversations?token={$token}",['form_params' => $sendchat])->getBody();
        $response= $this->ArrayResponse($response);
        return $response;
    }

}

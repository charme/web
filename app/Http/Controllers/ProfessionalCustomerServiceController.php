<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class ProfessionalCustomerServiceController extends Controller
{
    public function view_customer_service(){    	
        return view('professional_modules.customer_service');
    }
    public function send_message(request $request){
    	$supplier_id=Session::get('supplier_id');    	
        $supplier_token=Session::get('supplier_token'); 
    	//return $request->all();      
    	$message = array('subject'=>$request->title, 'message'=>$request->message);
 		$response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}/inbox?token={$supplier_token}",["form_params"=>$message])->getBody();
 		$response = $this->ArrayResponse($response);
 		//return $response;

 		if ($response['status']=='ok') {
 			return redirect('professional/customer_service/message')->with('status','Your message has been recieved, we will contact you shortly');
 		}
 		if ($response['status']=='error') {
 			return redirect('professional/customer_service/message')->with('error','Error Sending Message'+$response['error']['msg']);
 		}

 		else return redirect('professional/customer_service/message');
    }

    public function show_message(request $request){
    	return view('professional_modules/contact_form');
    }
}

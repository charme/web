<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PasswordController extends Controller
{
    public function password_recovery($token=null){ 
    	return view('login.recover_password',['token'=>$token]);
    }

    public function professional_password_recovery($token=null){ 
    	return view('professionals_login.recover_password',['token'=>$token]);
    }
}

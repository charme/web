<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class ProfessionalSigninController extends Controller
{
    public function welcome(){
    	 return view('professionals_login.signin');
    }

    public function login_request(Request $request){
    	$data = array();
    	$input=$request->all();
        //return $input;
    	/*Send username and password to API*/
    	$response = $this->charmeapi()->request('POST', 'suppliers/auth', ['form_params' => $input])->getBody();
		/*Convert response to Array*/
		$responseBody = (string) $response;
    	$responseBody = json_decode($responseBody, true);
        //return $responseBody;


        /*--------------------------Resume from here--------------------------------*/

    	/*Save response in variable*/
    	$data =array_add($data,'response', $responseBody);
    	/*Check for response status*/
    	
    	if($data['response']['status']==='ok'){

            /*Saves id and token used in middleware for logged-in*/
                Session::put('supplier_id',$data['response']['data']['Supplier']['id']);
                Session::put('supplier_token',$data['response']['data']['Supplier']['token']);

    		if($data['response']['data']['Supplier']['verified']==true){
    			Session::put('verified',true);
	    		return redirect('professional/services'); 
	    		/*Response status is Ok and account is verified*/
	    	}

            if($data['response']['data']['Supplier']['verified']==false){
            Session::put('signup_id',$data['response']['data']['Supplier']['id']);
            Session::put('signup_token',$data['response']['data']['Supplier']['token']);
                return redirect('register/professional/verify-phone')->with('error','Unverified Account');
            }
	    	//else return view('signin')->with('signin_error','Account Not Verified'); 
    	}

    	elseif ($data['response']['status']=='error') {
    		 return redirect('signin/professional')->with('signin_error',$data['response']['error']['msg']); 		
    	}

    }

    public function forgot_password(Request $request){
    	return view('professionals_login.forgot_password');
    }
    /*send email to reset password*/
    public function reset_password(Request $request){
    	$email=$request->email;
    	//return $email;
    	$response = $this->charmeapi()->request('POST', 'suppliers/password-recovery', ['form_params' => ['email'=> $email]])->getBody();
    	$response = $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {           
    	   return redirect('professional/forgot_password')->with('status','Email Sent');
        }
        else return redirect('professional/forgot_password')->with('error',$response['error']['msg']);
    }

    /*Change password*/
    public function remember_password(Request $request){
        $reset_token=$request->reset_token;
        if ($request->password !==$request->confirm_password) {
            return redirect("professional/password-recovery/{$reset_token}")->with('error','Password does not match'); 
        }
        $password_change = array('token' => $reset_token, 'new_password'=>$request->password);
        $response = $this->charmeapi()->request('POST', 'reset-password', ['form_params' =>$password_change])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
        if ($response['status']=='ok') {           
           return redirect('/signin/professional')->with('status','Password changed');
        }
        else return redirect("professional/password-recovery/{$reset_token}")->with('error',$response['error']['msg']);
    }

    public function resend_sms(Request $request){
        $id=Session::get('supplier_id');
        //return $request->session()->all();
        $token=Session::get('supplier_token');
        $data = array();
        /*Send verification SMS to user phone*/
        $verify_phone = $this->charmeapi()->request('GET', 'suppliers/'.$id.'/verify-phone?token='.$token)->getBody();
        $verify_phoneBody = (string) $verify_phone;
        $verify_phoneBody = json_decode($verify_phoneBody, true);
        $data= array_add($data,'verify-phone',$verify_phoneBody);
        //return $data;
            /*Check if verification code was successfully sent*/
            if($data['verify-phone']['status']==='ok' && !empty($data['verify-phone']['status'])){
                return redirect('register/professional/verify-phone')->with('status','SMS Sent.');
            }
            else
            $sms_error = array('error' => 'SMS verification sending failed.', 'id'=>$id, 'token'=>$token );
                return redirect('register/professional/verify-phone')->with('error','SMS sending failed.');
        
    }
}

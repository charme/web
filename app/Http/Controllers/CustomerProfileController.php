<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class CustomerProfileController extends Controller
{
    public function view_profile(){
    	$data = array();
    	$customer_id=Session::get('customer_id');
    	$response = $this->charmeapi()->request('GET', "customers/{$customer_id}")->getBody();
    	$response = $this->ArrayResponse($response);
        //return $response;
        /*$location = array_get($response, 'data.Customer.location');
        $location= $this->ArrayResponse($location);
        array_set($response,'data.Customer.location',$location['address_details']);*/
    	$data = array_add($data,'response',$response);
    	return view('modules.profile')->with('data', $data);
    }

    public function edit_profile(){
    	/*Get existing User Data and prepopulate the form*/
    	
    	$data = array();
    	$customer_id=Session::get('customer_id');
    	$response = $this->charmeapi()->request('GET', "customers/{$customer_id}")->getBody();
    	$response = (string) $response;
    	$response = json_decode($response,true);
    	$data = array_add($data,'response',$response);

    	return view('modules.edit_profile')->with('data', $data);
    }

 	public function update_profile(request $request){ 		
 		$updated_details=$request->all();
 		//return $updated_details;
 		$data = array();
    	$customer_id=Session::get('customer_id');

        $customer_token=Session::get('customer_token');
 		$response = $this->charmeapi()->request('POST', "customers/{$customer_id}?token={$customer_token}",["form_params"=>$updated_details])->getBody();
 		$response = (string) $response;
    	$response = json_decode($response,true);
    	$data = array_add($data,'response',$response);
 		//return $data;
 		if ($data['response']['status']==='ok') {
 			
    	return redirect('/profile')->with('profile_updated','Profile Successfully Updated');
 		}
        else return redirect('/profile/edit')->with('Update_failed','Profile Update Failed, Try Again');
    }   

    public function location(request $request){
        return view('modules.profile_location');
    }

    public function save_location(request $request){
        //return $request->all();
        /*save latlong*/
        $latlong = $request->location;
        if (!empty($request->address_details)) {
        $address_details=$request->address.'('.$request->address_details.')';
        }
        else $address_details=$request->address;
        $latlong = trim($latlong,'(');
        $latlong = trim($latlong,')');
        $location_split= explode(",", $latlong);
        $lat=(float) trim($location_split[0]);
        $long=(float) $location_split[1];

        $save_address = array('latitude' => $lat,'longitude'=>$long, 'address_details'=> $address_details);
        $save_address=json_encode($save_address);
        //return $save_address;
        $customer_id=Session::get('customer_id');
        $customer_token=Session::get('customer_token');
        $location = array('location' => $save_address, );
        $response = $this->charmeapi()->request('POST', "customers/{$customer_id}?token={$customer_token}",["form_params"=>$location])->getBody();
        $response = $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return redirect('/profile')->with('status','Location saved');
        }
        if ($response['status']=='error') {
            return redirect('/profile')->with('error',"Location not saved, {$response['error']['msg']}");
        }
        else return redirect('/profile')->with('error',"Something went wrong");//
    } 

     public function update_dp(request $request){
        $token=session('customer_token');
        $id=session('customer_id');
        $type=$request->file('pics')->getMimeType();
        //return $type;
         $pics = array('pics' => $request->file('pics'));
        $uri=getenv("CHARME_API");
         $response = \Httpful\Request::post($uri."customers/$id/profile-pics?token=$token")->expectsJson()
         ->addHeader('Accept', "application/json")
         ->addHeader('Content-Type', "multipart/form-data");
         $response=$response->attach($pics);
         $response=$response->send();
        $response= json_decode((string) $response,true);
        //return $response;
        if ($response['status']=='ok') {
            return redirect('/profile')->with('status','Profile picture changed');
        }
        if ($response['status']=='error') {
            return redirect('/profile')->with('error',"Profile picture not changed, {$response['errror']['msg']}");
        }
        else return redirect('/profile');
    }   
}

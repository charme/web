<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use Session;

class ProfessionalServiceController extends Controller
{
    public function view_services () {  

    	$supplier_id=Session::get('supplier_id');
    	$response = $this->charmeapi()->request('GET', "suppliers/$supplier_id/services")->getBody();
    	//return $response; 
    	$services = $this->ArrayResponse($response);
    	//$services=$services['data']['SupplierServices'];
    	$services=$services['data'];
    	//return $services;
        return view('professional_modules.my_services',['services'=>$services]);
    }

    public function save_services(request $request){
        $supplier_id=Session::get('supplier_id');
        $supplier_token=session('supplier_token');
        $service_availability= $request->all();
        $service_availability=json_encode($service_availability);
        //print_r($service_availability);
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}/available?token={$supplier_token}",["json"=>['service_availability'=>$service_availability]])->getBody();
        $response = $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return redirect('professional/services')->with('status','Service Saved');
        }

        else return redirect('professional/services')->with('error','Service were not saved, try again');
    }

    public function toggle_available(request $request){
        $available = array('availability' => $request->available);
        $supplier_id=Session::get('supplier_id');
        $supplier_token=session('supplier_token');
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}/available?token={$supplier_token}",["form_params"=>$available])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
    }
}

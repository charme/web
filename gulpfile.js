// include gulp
var gulp = require('gulp'); 

// include plug-ins
var jshint = require('gulp-jshint');

// JS hint task
gulp.task('jshint', function() {
  gulp.src('./public/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
 
gulp.task('jsmin', function () {
	gulp.src('./public/js/*.js')
		.pipe(jsmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./public/min/'));
});

var concat = require('gulp-concat');
 
gulp.task('catenate', function() {
  return gulp.src('./public/js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/min/'));
});
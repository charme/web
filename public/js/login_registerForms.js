

          function recoverAccount(){
            var password=$('#password').val();
            var confirm_password=$('#confirm_password').val();
            if (password=='') {
              //alert('enter Password');
              return false;
            };
            if (confirm_password=='') {
              //alert('Confirm Password');
              return false;
            };
            if (password != confirm_password) {
              //alert('password doesnt match');
              return false;
            }
          }

          /*Show password field*/
          function showPassword(in_field){
            var id = $(in_field).prevAll('input').attr("id");
            var type = $(in_field).prevAll('input').attr("type");
              if (type=='password') {
              document.getElementById(id).type="text"; 
              $(in_field).html('<i class="fa fa-eye-slash"></i>');
              ////console.log(type+" = " +id );        
              }

              if (type=='text') {
              $(in_field).html('<i class="fa fa-eye"></i>');
              document.getElementById(id).type="password"; 
              ////console.log(type);              
              }
          }
        /*Validate recovery password*/
          
        /*Validation for password change*/
        var validate_resetPassword= new FormValidator('resetPassword',
          [
            {
              name: 'password',
              display: 'Enter new password',
              rules: 'required|alpha_numeric|min_length[6]'            
            },
            {
              name: 'confirm_password',
              display: ' Enter confirmed password',
              rules: 'required|alpha_numeric|matches[password]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              $('.password_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
        validate_resetPassword.setMessage('required', 'Please %s.');
        validate_resetPassword.setMessage('matches[password]', 'Password does not match.');

          /*Validate login Form*/
        var validate_login_form= new FormValidator('customerSignIn',
          [
            {
              name: 'username',
              display: 'Email or Phone Number',
              rules: 'required'
            },
            {
              name: 'password',
              rules: 'required|min_length[6]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.form_errors').html(errors[0].message);
              if (errors[0].display!='Gender') {
                errors[0].element.focus();                
              };
              return false;
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_login_form.setMessage('required', 'Kindly enter %s.');
        
        /*Validate Registration form*/

        var validate_register_form= new FormValidator('registerCustomer',
          [
            {
              name: 'first_name',
              display: 'First Name',
              rules: 'required|alpha'
            },
            {  
              name: 'last_name',
              display: 'Last Name',
              rules: 'required|alpha'
            },
            {
              name: 'gender',
              display: 'Gender',
              rules: 'required'
            },
            {
              name: 'email',
              display: 'email',
              rules: 'required|valid_email'
            },
            {
              name: 'phone_no',
              display: 'Phone Number',
              rules: 'required|numeric'
            },
            {
              name: 'password',
              display: 'Password',
              rules: 'required|min_length[6]',
              depends: function(){
                var facebook=$('#fb_id').val();
                var google=$('#g_token').val();
                console.log(facebook);
                if (facebook !='' || google!='') {
                  return false;
                };
                if (facebook =='' || google=='') {
                 return true;
                };
              }
            },
            {
              name: 'confirm_password',
              display: 'Confirm Password',
              rules: 'required|min_length[6]|matches[password]',
              depends: function(){
                var facebook=$('#fb_id').val();
                var google=$('#g_token').val();
                console.log(facebook);
                if (facebook !='' || google!='') {
                  return false;
                };
                if (facebook =='' || google==''){
                 return true;
                };
              }
            },
            {
              name: 'reference_code',
              display: 'Reference code',
              rules: 'exact_length[5]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.form_errors').html(errors[0].message);
              if (errors[0].display!='Gender') {
                errors[0].element.focus();                
              };
              return false;
              event.preventDefault(); 
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_register_form.setMessage('required', '%s is required.');
        validate_register_form.setMessage('valid_email', 'Please use a valid %s.');
        validate_register_form.setMessage('matches[password]', 'Password does not match.');
        validate_register_form.setMessage('max_length[7]', 'Please select a Gender');
        validate_register_form.setMessage('exact_length[5]', '%s must be 5 characters');


        /*Phone verification form*/
        var validate_verify_form= new FormValidator('verifyPhone',
          [
            {
              name: 'code',
              display: 'verification Code',
              rules: 'required|alpha_numeric'
            }            
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.form_errors').html(errors[0].message);
              errors[0].element.focus();
              return false;
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_verify_form.setMessage('required', 'verification code is required.');
         /*Validate forgot password Form*/
        var validate_forgot_password_form= new FormValidator('forgot_password',
            [
              {
                name: 'email',
                rules: 'required|email'
              }
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                $('.form_errors').html(errors[0].message);
                errors[0].element.focus();
                return false;
              };

            });
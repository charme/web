var pusher = new Pusher('d9a5d6f1f32449da826d', {
          encrypted: true
        });
        var current_id=$('#current_user').attr('data-id');
        var user_channel='charme-supplier-'+current_id;
        var channel = pusher.subscribe(user_channel);
          /*
             DONE const CHAT = 'push_chat'; Customer
             DONE const APPOINTMENT_START = 'push_appointment_start'; Customer / SUpplier
             DONE const APPOINTMENT_STOP = 'push_appointment_stop'; Customer
             DONE const APPOINTMENT_CANCEL = 'push_appointment_cancel'; Supplier
             DONE const APPOINTMENT_RESCHEDULE = 'push_appointment_reschedule'; Supplier
             DONE const INBOX = 'push_inbox'; Customer / Supplier
             const APPOINTMENT_REMINDER = 'push_appointment_reminder'; Customer / Supplier
             const LOCATION = 'push_location'; Customer / Supplier
             const APPOINTMENT_NEW = 'push_appointment_new'; Customer / Supplier
          */
          /*-----------------Chat message Bind------------*/
          /*listens to chat messages*/
          var chat_app_id=$('#scrollTo').attr('data-id');
            channel.bind('push_chat', function(data) {
                //console.log(data);
              if (data.appointment_id==chat_app_id && data.sent_by=='customer') {
                var chat_message='<li class="message-right" onclick="showMsgDetails(this)">'+
                        '<img src="'+chat_img+'" title="'+data.sender_name+'" class="minilogo mdl-shadow--2dp">'+
                        '<div class="message mdl-shadow--2dp">'+
                          '<p>'+data.message+'</p>'+                  
                        '</div>'+
                        '<div id="msg_detail" class="text_left">'+
                        '<small data-livestamp="'+data.time_sent+'"></small>'+
                        '</div>'+
                      '</li>';
                $('#appointment_chat').append(chat_message);
              }
              if (data.appointment_id!=chat_app_id) {
                    /*Show alert of message from another customer*/
                    $('.live_updates').fadeIn('fast');
                    $('.live_updates').html('<span class="update_close">'+
                                              '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                        '</span>'+
                                        '<a href="'+chat_url+data.appointment_id+'">'+
                                          '<div class="update_img">'+      
                                            '<img src="'+img+'">'+
                                          '</div>'+
                                          '<div class="update_content">'+
                                            '<small class="update_title bold">'+
                                              data.sender_name+
                                            '</small>'+
                                            '<small class="update_msg">'+
                                              data.message+
                                            '</small>'+        
                                          '</div>'+
                                        '</a>');
                    $('.live_updates').delay(8000).fadeOut('fast');
                  }                 
          }); 
        
          /*--------------Inbox Bind------------*/
          channel.bind('push_inbox', function(data) {
            //alert('new inbox');
            console.log(data);
             $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+inbox_url+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            data.sent_by+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            shortText(data.message)+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast'); 
          });
          /*APPOINTMENTS*/
          channel.bind('push_appointment_new', function(data) {
            alert('new appointment');
            console.log(data);
          });

          /*----Appointment start bind--------*/
          channel.bind('push_appointment_start', function(data) {            
            console.log(data);
             $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+appointment_url+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            'Appointment'+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            'You just started an appointment.'+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast'); 
                    $("img#"+data.appointment_id).attr('data-time',data.time);
          });
          /*------Appointment stop bind--------*/
          channel.bind('push_appointment_stop', function(data) {
            console.log(data);
            $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="#">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            'Appointment'+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            'Great Job!, appointment completed.'+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast'); 
          });
          /*-------Appointment cancel bind-------------*/
          channel.bind('push_appointment_cancel', function(data) {
            //alert('appointment cancelled');
            $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+appointment_details_url+data.appointment_id+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            'Appointment Cancelled'+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            'An appointment was just cancelled.'+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast'); 
            
            console.log(data);
          });
          /*---------Appointment reschedule bind-------*/
          channel.bind('push_appointment_reschedule', function(data) {
            console.log(data);
            $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+appointment_details_url+data.appointment_id+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            'Appointment Reschedule'+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            'An appointment was just rescheduled.'+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast'); 
            //alert('appointment rescheduled');
          });
          /*--------Appointment reminder bind---------*/
          channel.bind('push_appointment_reminder', function(data) {
            alert('appointment reminder');
            console.log(data);
          });
        /*Callbacks when pusher is connected, connecting, unavailable,failed and disconnected*/
                pusher.connection.bind('connected',function(){
                  console.log('pusher connected');
                  //$('.chat').css('background-color','#F1F4F5');
                });

                pusher.connection.bind('connecting',function(){
                  console.log('pusher connecting');
                  $('#send_button').attr('disabled');
                  //$('.chat').css('background-color','#FFF9C4');
                });

                pusher.connection.bind('unavailable',function(){
                  console.log('pusher unavailable');
                  //$('.chat').css('background-color','#E57373');
                });

                pusher.connection.bind('failed',function(){
                  console.log('pusher failed');
                  //$('.chat').css('background-color','#E57373');
                });

                pusher.connection.bind('disconnected',function(){
                  console.log('pusher disconnected');
                  //$('.chat').css('background-color','#E57373');
                });
        /*End of Callbacks to pusher connected*/
  function fb_login() {
    FB.login( function(response) {console.log(response);}, { scope: 'email,public_profile',return_scopes: true } );
  }
// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
        FB.login( function(response) {console.log(response);testAPI();}, { scope: 'email,public_profile',return_scopes: true } );
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
        FB.login( function(response) {console.log(response);testAPI();}, { scope: 'email,public_profile',return_scopes: true } );
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '508448699363738',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.5' // use graph api version 2.5
    });
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me',{fields:['last_name','first_name','name','email','gender','picture']}, function(response) {
      console.log(JSON.stringify(response));
      $('#'+response.gender).attr("checked", 'checked');
      $('#'+response.gender).parent().addClass('is-checked');
      $('#first_name').val(response.first_name);
      $('#pic_url').val(response.picture.data.url);
      $('#first_name').parent().addClass('is-focused');
      $('#last_name').val(response.last_name);
      $('#last_name').parent().addClass('is-focused');
      $('#fb_id').val(response.id);
      $('#email').val(response.email);
      $('#email').parent().addClass('is-focused');
      $('#password').parent().hide('slow');
      $('#confirm_password').parent().hide('slow');
      $('#social_alerts').text('Hi '+response.name+', welcome to Charmé').fadeIn('fast');
    });
  }
    /*FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
        console.log(response);
        console.log(response.first_name);
        $('#status').append(response);
    });*/
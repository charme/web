    @extends('layouts.professional_header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Inbox -->
                <div class="central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">                    
                    @if(isset($inbox))
                      @foreach($inbox as $message)
                       <div id="{{$message['id']}}" class="text_left mdl-card__actions mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone border_round mdl-shadow--2dp">                        
                        <span class="bold mdl-color-text--black big">Charme Admin</span>
                        <span class="bold mdl-color-text--black">{{$message['subject']}}</span>
                        <span class="">‎{{$message['message']}}</span>
                      </div>
                      @endforeach
                    @endif
                    @if(empty($inbox))
                    <i class="fa fa-inbox fa-5x"></i>
                    <h4>You have no messages yet.</h4>
                    @endif
                    <a href="{{url('professional/customer_service/message')}}" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Contact Admin <i class="fa fa-share-square-o"></i></a>      
                  </div>
                </div>
                  <!-- /Inbox -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
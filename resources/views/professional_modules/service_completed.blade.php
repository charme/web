       @extends('layouts.sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <h4>Mission Accomplished!</h4>                     
                    <i class="fa fa-star-o fa-5x green-text"></i>
                    <div class="bold">
                      <p>Good job done!</p>
                      <p>Rate the client and give feedback</p>
                    </div>
                    <form action="{{url('/professional/appointments/end')}}" name="endService" method="post">
                      <div class="central mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                       <div class="stars">
                          <input class="star star-5" id="star-5" type="radio" name="rating" value="5" />
                          <label class="star star-5" for="star-5"></label>
                          <input class="star star-4" id="star-4" type="radio" name="rating" value="4"/>
                          <label class="star star-4" for="star-4"></label>
                          <input class="star star-3" id="star-3" type="radio" name="rating" value="3"/>
                          <label class="star star-3" for="star-3"></label>
                          <input class="star star-2" id="star-2" type="radio" name="rating" value="2"/>
                          <label class="star star-2" for="star-2"></label>
                          <input class="star star-1" id="star-1" type="radio" name="rating" value="1"/>
                          <label class="star star-1" for="star-1"></label>
                        </div>
                      </div>
                      <div class="clr"></div>
                      <input name="id" value="{{$id}}" hidden>
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-cell--12-col" name="comment" type="text" id="comment">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="comment">Your Feedback here</label>
                      </div>
                      @include('common.form_errors')
                      <input type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
                    </form>                     
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
       @extends('layouts.professional_header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white">
        <section class=""> <!-- first tab -->
          <div class="mdl-grid">
            @include('common.earnings_errors')
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp central mdl-color--white">
                      <div class="bg-212121 bold mdl-color-text--white">Account Details
                          <img class="width--36" src="{{URL::asset('icon/edit-48.png')}}" alt="" id="edit_account" />
                      </div>             
                      <div class="central mdl-color--white">
                        <form name="account_form" method="post" action="{{url('professional/earnings/account')}}">
                          <span class="bold mdl-color-text--black">                          
                            Bank Name: <input class="border_bottom--none inherit mdl-textfield__input line" type="text" id="bank_name" value="{{session('Professional')->bank_name ? session('Professional')->bank_name: ''}}" name="bank_name">
                          </span><br>
                          <span class="bold mdl-color-text--black">
                            Account Number: <input class="border_bottom--none inherit mdl-textfield__input line" type="text" id="account_no" value="{{session('Professional')->account_no ? session('Professional')->account_no: ''}}" name="account_no">
                          </span><br>
                          <p class="account_errors mdl-color-text--red bold"></p>
                          <button id="save_bank_details" type="submit" class="none mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                            Update
                          </button>
                        </form>
                      </div>          
                </div>
            @if(!empty($earnings['data']['Earnings']))
              @foreach($earnings['data']['Earnings'] as $income)
                <div class="earnings mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp bold">
                  <div class="bottom-margin--1em central big"> 
                    <span class="border_bottom--pink">{{$income['pretty_date']}}</span>
                  </div><br>
                  <span class="">Total Earnings</span>
                  <h4>₦{{number_format($income['total_amount']/100)}}</h4>            
                  <div class="mdl-card__actions no_padding">
                    <div class="mdl-layout-spacer"></div>
                    <div class="right mdl-color--pink padding--5px">
                      <a href="{{url('/professional/earnings/breakdown/')}}/{{$income['date']}}" class="one-third mdl-color-text--white">View Breakdown
                      </a>                      
                    </div>
                  </div>
                </div>
              @endforeach
            @endif

          </div>
        </section>
      </main>
    @endsection
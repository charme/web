       @extends('layouts.professional_header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
              @if(isset($services))
                @if(!empty($services['SupplierServices']))
                  @foreach($services['SupplierServices'] as $key => $category)
                    <div class="central mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <div class="mdl-card mdl-shadow--2dp text_left">
                        <span class="bold padding--7px service">
                          <i class="fa fa-angle-down fa-2x mdl-color-text--pink cancel"></i>  {{$key}}
                          <input type="checkbox" class="right" name="make_up"/>
                        </span>

                        <div class="bg-grey padding--7px bold">
                          @foreach($category as $service)
                            <span class="padding--7px border_bottom service_details">
                              {{$service['name']}}                              
                              <input type="checkbox" class="right" name="{{$service['name']}}"/>
                            </span>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  @endforeach
                @endif
              @endif

                <!-- <div class="central mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp text_left">

                    <span class="bold padding--7px service">
                      <i class="fa fa-angle-down fa-2x mdl-color-text--pink onclick-menu cancel" id="1"></i>  Barber 
                      <input type="checkbox" class="right" checked name="barber"/>
                    </span>

                    <div class="bg-grey padding--7px bold onclick-menu-content" id="sub1">
                      <span class="padding--7px border_bottom service_details">
                        Smokey eyes <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p> 
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                      <span class="padding--7px border_bottom service_details">
                        Rep lips <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p>  
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="central mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp text_left">

                    <span class="bold padding--7px service">
                      <i class="fa fa-angle-down fa-2x mdl-color-text--pink onclick-menu cancel" id="2"></i>  Hairdressing 
                      <input type="checkbox" class="right" name="make_up"/>
                    </span>

                    <div class="bg-grey padding--7px bold onclick-menu-content" id="sub2">
                      <span class="padding--7px border_bottom service_details">
                        Smokey eyes <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p> 
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                      <span class="padding--7px border_bottom service_details">
                        Rep lips <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p>  
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="central mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp text_left">

                    <span class="bold padding--7px service">
                      <i class="fa fa-angle-down fa-2x mdl-color-text--pink onclick-menu cancel" id="3"></i>  Spa 
                      <input type="checkbox" class="right" name="make_up"/>
                    </span>

                    <div class="bg-grey padding--7px bold onclick-menu-content" id="sub3">
                      <span class="padding--7px border_bottom service_details">
                        Smokey eyes <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p> 
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                      <span class="padding--7px border_bottom service_details">
                        Rep lips <p class="mdl-color-text--pink border-pink line cancel" id="set_time">SET TIME</p>  
                        <input type="checkbox" class="right" name="make_up"/>
                      </span>
                    </div>

                  </div>
                </div> -->
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
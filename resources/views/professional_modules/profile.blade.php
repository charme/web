       @extends('layouts.professional_header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Basic Info & Pic -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                  <!-- Notification on profile update -->
                    @include('common.errors')
                    @if (session('profile_updated'))
                    <div class="mdl-color-text--white mdl-color--pink mdl-card">
                       {{ session('profile_updated') }} 
                    </div> 
                    @endif
                    @if (isset($data))                  
                    <div class="profile_pic">
                      <img src="{{$data['response']['data']['Supplier']['pic_url'] ? $data['response']['data']['Supplier']['pic_url'] : asset('img/user.jpg')}}" alt="" />
                    </div>
                    <h4>{{$data['response']['data']['Supplier']['first_name']}} {{$data['response']['data']['Supplier']['last_name']}} </h4>
                    <h6>{{$data['response']['data']['Supplier']['phone_no']}}</h6>
                    <h6>{{$data['response']['data']['Supplier']['email']}}</h6>
                  </div>
                </div>
                  <!-- /Basic Info & Pic -->


                  <!-- About You -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <div class="">
                            <h4 class="bold">About You                                
                                <span id="change_description" class="line-block mdl-color-text--pink fa-stack">
                                  <i class="fa fa-square-o fa-stack-2x"></i>
                                  <i class="fa fa-pencil fa-stack-1x"></i>
                                </span>
                            </h4>                
                            <div class="border-pink--1px padding--5px" id="description_holder">
                              <p class="bold text-justify" id="description">{{$data['response']['data']['Supplier']['description'] ? $data['response']['data']['Supplier']['description'] : ''}}</p>
                              
                            </div> 
                    </div><div id="demo"></div>
                  </div>
                </div>
                  <!-- /ABout you -->
              </div>

                  <!-- Location Details -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <div class="left">
                            <h4 class="bold"><i class="material-icons mdl-color-text--pink">location_on</i> Locations <a href="{{url('professional/profile/location')}}"><i class="material-icons mdl-color-text--pink">edit</i></a></h4>
                            <div class="clr border_bottom"></div>                  
                            <ul>
                              <li>
                                {{$data['response']['data']['Supplier']['location']['address_details'] ? $data['response']['data']['Supplier']['location']['address_details'] : ''}}         
                              </li>
                            </ul>     
                    </div>
                  </div>
                </div>
                  <!-- /Location Details -->
            </div>
          </div>
        </main>
                @endif

        <!-- /Page Content -->
    @endsection
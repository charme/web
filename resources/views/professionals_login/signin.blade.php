       @extends('layouts.professionals_login_register')
       @section('content')
    <!-- Page Content -->
        
          <main class="contact-about white_bg">
            <div class="mdl-color-white central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
              <form action="{{url('/professional_signin')}}" name="professionalSignIn" method="POST">
              @include('common.professional_login_error')
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <input class="mdl-textfield__input" type="text" id="username" name="username">
                  <label class="mdl-textfield__label bold mdl-color-pink central" for="username">Email address or Phone number</label>
                </div> 
                         
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <input class="mdl-textfield__input " type="password" id="password" name="password">
                  <label class="mdl-textfield__label bold mdl-color-pink central" for="password">password</label>
                </div>
                @include('common.form_errors')
                <button id="signin" type="submit" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Sign in</button>      
              </form>
              <div class="top-margin--1em bottom-margin--1em"><a href="{{url('professional/forgot_password')}}" class="mdl-color-text--black bold">Forgot password?</a></div>            
            </div>
          </main>
        <!-- /Page Content -->

    @endsection
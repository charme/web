       @extends('layouts.sub_page')
       @section('content')
    <!-- Page Content -->
        
          <main class="mdl-layout__content">  
          <div class="contact-about mdl-color--white">
            <div class="mdl-card mdl-shadow--2dp about overflow--visible">
              <div class="mdl-grid">
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  @if(session('registration_successful'))
                    <div class="mdl-color-text--white mdl-color--green bold mdl-card">
                       {{ session('registration_successful') }} 
                    </div> 
                  @endif
                  <form action="{{url('register/professional/more')}}" name="moreDetails" method="POST">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <textarea class="mdl-textfield__input" type="text" name="description" rows= "3" id="description" ></textarea>
                      <label class="mdl-textfield__label bold" for="description">Tell us about yourself</label>
                    </div> 
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <div class="bold text_left">What service(s) do you offer</div>
                      <div  class="mdl-grid">
                      <!-- 1st half -->
                        <div class="mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone ">
                          <div id="cat_list">
                            <div class="dropdown mdl-cell--12-col text_left">
                              <select id="select_category" onchange="loadService(this.value,$(this).text())" class="mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone select" name="service_category">
                                <option id="null" value="null">Select service category</option>
                                @foreach($categories as $category)
                                  <option id="{{$category["id"]}}" value="{{$category["id"]}}">{{$category["name"]}}</option>
                                @endforeach
                              </select>
                            </div> 
                          </div>
                          <span id='new_service' class="mdl-color-text--pink text_left new_service">Add another service</span>
                        </div>

                        <!-- 2nd Half -->
                        <div id="services_select" class="dropdown mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone text_left">                          
                          
                        </div>
                      </div>                                                 
                    </div>
                    @include('common.form_errors')
                    <button type="submit" id="create" class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Submit</button>      
                  </form>
              
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>
        <!-- /Page Content -->

    @endsection
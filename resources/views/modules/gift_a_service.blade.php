       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="">
              <div class="mdl-grid">
                  <!-- Invite a friend -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    @include('common.errors')
                    <div class="gift_service">
                      <img src="{{asset('img/charme_icons/gift-a-service.png')}}" class="width--100px">
                    </div>
                    <h4>Enter your friend's details</h4>  
                    <div class="bold">
                      <form action="{{url('gift/save_receiver')}}" name="giftService" method="post">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" id="name" name="name">
                          <label class="mdl-textfield__label" for="name">Name</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" id="email" name="email">
                          <label class="mdl-textfield__label" for="email">Email address</label>
                        </div>


                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" pattern="[+,0-9]*" id="phone" name="phone">
                          <label class="mdl-textfield__label" for="phone">Phone Number</label>
                        </div>
                        @include('common.form_errors')
                        <input type="submit" value="Select Service" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/> 
                      </form>
                    </div>
                         
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
<!DOCTYPE html>
<html>
<head>
      <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
      <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
      
      <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('tour/slick-theme.css')}}"> -->
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<!--<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
<script type="text/javascript" src="{{URL::asset('tour/slick.min.js')}}"></script>
	<title>Tour</title>
	<style type="text/css">
		.slick-next {
			position: fixed;
	    	bottom: 0px;
	    	right: 0px;

		}
		.slick-prev {
			position: fixed;
	    	bottom: 0px;
	    	left: 0px;
		}
		.img_content{
			margin: auto;
			height: 25%;
		}
		div{
			text-align: center;
		}
		@media only screen and (min-width: 480px) {
			.img_body{
				height:70%;
				margin:auto;
			}
			.main_img{
				width: 20%;
				margin: auto;
				vertical-align: middle;
		    	display: table-cell;
			}
			.sub_img{
			    width: 25%;
			    margin: auto;
			    vertical-align: middle;
			    display: table-cell;
			}
			.content{
				text-align: center;
				width: 50%;
				margin:auto;
				font-size: x-large;
			}
			.content b{
				font-style: italic;
				font-size: x-large;
			}
			.body{
				width:100%;
				margin:auto;
			}
			#hi{
				margin-bottom: 1em;
			}
		}
		@media only screen and (max-width: 480px) {
		.img_body{
				height:60%;
				margin:auto;
			}
		.main_img{
				height: 70%;
    			width: 55%;
				margin: auto;
				vertical-align: middle;
		    	display: table-cell;
			}
		.sub_img{
			    width: 50%;
			    margin: auto;
			    vertical-align: middle;
			    display: table-cell;
		}
		.content{
				text-align: center;
				width: 90%;
				margin:auto;
				font-size: 1em;
			}
		.content b{
				font-style: italic;
				font-size: 1em;
			}
		.body{
				width:100%;
				margin:auto;
			}
		#hi{
				margin-bottom: 1em;
			}
		}
		h1,h2,h3,h4,h5,h6{
			font-weight: bold;
			margin: 0px;
		}
		body{
			overflow: hidden;
		}
	</style>
</head>
<script type="text/javascript">
	$(document).ready(function(){
	  $('.body').slick({
	    autoplay:false,
	    autoplaySpeed: 8000,
	    draggable: true,
	    infinite: false
	  });
	});
</script>
			<!-- <div class="img_body">			
				<img src="{{asset('tour/tosend1.png')}}" class="main_img">
			</div>
			<div class="content">
				<span id="hi" class="mdl-color-text--pink">
					Hi {{session('Customer')->first_name? session('Customer')->first_name : ' '}}, my name is Kes.
				</span>
				<div class="mdl-color-text--pink">
					You can get <b>charmed</b> in just a few steps!
				</div>
			</div> -->

			<!-- <div class="img_content">			
				<img src="{{asset('tour/tosend2.png')}}" class="sub_img">
			</div>
			<div class="content">
				<h5 class="mdl-color-text--pink">
					Select A Service
				</h5>
				<div class="img_content">			
					<img src="{{asset('tour/screenshots/services.jpg')}}" class="sub_img">
				</div>
			</div> -->

<body>
	<div class="body">
		<!-- Slide 1 -->
		<div>
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<img src="{{asset('tour/tosend1.png')}}" class="main_img">			  	
			  	</div>
			  	<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--pink">
					Hi {{session('Customer')->first_name? session('Customer')->first_name : ' '}}, my name is Kes.
					<br>
					You can get <b>charmed</b> in just a few steps!
				</div>
			</div>
		</div>
		<!-- Slide 2 -->
		<div>
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<img src="{{asset('tour/tosend2.png')}}" class="sub_img">
			  	</div>
			  	<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--pink">
					<h5 class="mdl-color-text--pink">
						Select A Service
					</h5>
					<div class="img_content">			
						<img src="{{asset('tour/screenshots/services.jpg')}}" class="sub_img">
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 3 -->
		<div>
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<img src="{{asset('tour/tosend2.png')}}" class="sub_img">
			  	</div>
			  	<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--pink">
					<h5 class="mdl-color-text--pink">
						Select Professional
					</h5>
					<div class="img_content">			
						<img src="{{asset('tour/screenshots/professionals.jpg')}}" class="sub_img">
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 4 -->
		<div>
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<img src="{{asset('tour/tosend2.png')}}" class="sub_img">
			  	</div>
			  	<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--pink">
					<div class="img_content">			
						<img src="{{asset('tour/screenshots/appointment.jpg')}}" class="sub_img">
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 5 -->
		<div>
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<img src="{{asset('tour/tosend2.png')}}" class="sub_img">
			  	</div>
			  	<div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--pink">
			  		<h4 class="mdl-color-text--pink">
						Make Payment
					</h4>
					<div class="img_content">			
						<img src="{{asset('tour/screenshots/payment.jpg')}}" class="sub_img">
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="">
              <div class="mdl-grid">
                  <!-- Invite a friend -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    @include('common.errors')
                    <div class="gift_service">
                      <img src="{{asset('img/charme_icons/redeem-a-gift.png')}}" class="width--100px">
                    </div>
                    <h4>Enter Your Gift Card</h4>  
                    <div class="bold">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" id="gift_code" name="code">
                          <label class="mdl-textfield__label" for="code">Gift Code</label>
                        </div>
                        <div id="results"></div>
                        <button type="button" id="redeem_gift" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> 
                        REDEEM
                        </button>
                    </div>
                         
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
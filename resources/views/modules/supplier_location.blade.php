       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp"> 
                    <p id="demo"></p>                    
                    <div id="map_section" class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <div id="map" style="height:380px;" class="mdl-card mdl-shadow--2dp">                    
                      </div>
                    </div>
                    <a href="{{url('appointments/chat')}}/{{session('appointment_id')}}" 
                      class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                      Chat with {{$data['Supplier']['first_name']}}
                    </a>      
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid service_style">
                @include('common.errors')
                @foreach($response['data']['Services'] as $service)
                  <div id="{{$service['id']}}" class="pink_hover mdl-card__actions mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone border_round mdl-shadow--2dp">
                      <span class="left mdl-color-text--black">{{$service['name']}}</span>
                      <span class="right bold line">‎₦{{number_format($service['price']/100)}}</span>
                </div>
                @endforeach                
              </div>
            </div>
                <div class=" hidden mdl-cell mdl-cell--12-col central mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <h4><i class="fa fa-clock-o fa-2x mdl-color-text--pink" aria-hidden="true"></i> Set Your Appointment Time</h4>
                  <form method='get' action="{{url('/services/professionals')}}">
                    <input id='appdatetimepicker' class='mdl-color--white' type='text' value='Set Date & Time' placeholder='Select Time' name='appointment_time'>
                    <input type='text' id="service_id" name='service_id' value="" >
                    <div id='ad'></div>
                    <div style='display:block;'>
                      <!-- Colored raised button -->
                      <button type='submit' class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                        Continue
                      </button>
                    </div>
                  </form>
                </div>
          </div>
        </main>
        <!-- /Page Content -->
    @endsection
    
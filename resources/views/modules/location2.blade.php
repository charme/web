       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">                      
              <div id="holder" class="mdl-grid">
                  <!-- Extra info & address -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <form action="{{url('services/details')}}" method="POST">
                      <hr class="border-top">
                      <h4 class="">Appointment Instruction.</h4> 
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" name="instructions" placeholder="Bring along..." type="text" id="instructions">
                          <label class="mdl-textfield__label bold mdl-color-pink" for="instructions"></label>
                        </div>
                      <hr class="border-top">
                      <h4 class="">Appointment Address.</h4>                     
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" id="address" name="address">
                          <input type="text" name="location" id="location" hidden>
                          <label class="mdl-textfield__label bold mdl-color-pink" for="address">(Enter Address Here)</label>
                        </div>
                        @if(!empty(session('Customer')->location))
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="my_location">
                            <input type="radio" id="my_location" class="mdl-radio__button" 
                              data-address="{{session('Customer')->location->address_details}}" 
                              data-log='{{session('Customer')->location->longitude}}' 
                              data-lat='{{session('Customer')->location->latitude}}'
                              name="saved_location">
                            <span class="mdl-radio__label bold">My Location</span>
                          </label>
                        </div>
                        @endif
                        @if(!empty($data['Supplier']['location']))
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                          <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="supplier_address">
                            <input type="radio" id="supplier_address" class="mdl-radio__button" 
                              data-address="{{$data['Supplier']['location']['address_details']}}" 
                              data-log='{{$data['Supplier']['location']['longitude']}}' 
                              data-lat='{{$data['Supplier']['location']['latitude']}}'
                              name="saved_location">
                            <span class="mdl-radio__label bold">{{strtolower($data['Supplier']['first_name'])}}'s location</span>
                          </label>
                        </div> 
                        @endif

                        <p id="location_found" class="padding--5px border-radius--5px"></p>
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                        <input class="mdl-textfield__input " type="text" id="address_details" name="address_details">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="address_details">(Please Provide Other Details Describing your Address)</label>
                      </div>            
                      <button type="button" id="save_details"  class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Use Location <i class="fa fa-arrow-right"></i></button>      
                    </form>
                    <div id="demo" class="mdl-color--red mdl-color-text--white padding--5px border-radius--5px" style="display:none;"></div>
                    <div id="error"></div>                        
                  </div>
                </div>
                  <!-- /Extra info & address -->
                  <!-- Location Details -->
                <div id="map_section" class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div id="map" style="height:380px;" class="mdl-card mdl-shadow--8dp">                    
                  </div>
                </div>
                  <!-- /Location Details -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection

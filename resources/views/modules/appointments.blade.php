       @extends('layouts.header')
       @section('content')
       <?php 
       function date_app($start_at){
        $app_date=date_create($start_at);
        $today=date_create(date("Y-m-d h:i:s"));
        if ($today==$app_date) {
          echo 'Today';
        }
        if ($today>$app_date) {
          echo " ";
        }

        if ($today<$app_date) {
          $diff=$today->diff($app_date,false);
          $diff= $diff->format("%a");
          if ($diff==0) {
            echo "Today";
          }
          if ($diff !=0) {
            echo "{$diff} days left";
          }
        }
        else echo " ";
       }
       ?>
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white">
        <!-- Upcoming Appointments -->
        <section class="mdl-layout__tab-panel is-active" id="scroll-tab-1"> <!-- first tab -->
          <div class="mdl-grid" id="upcoming_appointments">
            <!-- Notification of ended appointment -->
            @if(!empty($data['past']['data']['Appointments']))
              @foreach($data['past']['data']['Appointments'] as $past)
                @if($past['Appointment']['supplier_end_at'] && !$past['Appointment']['customer_rating'] && !$past['Appointment']['cancelled'])
                  <a href="{{url('/appointments/completed')}}/{{$past['Appointment']['id']}}" 
                  class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    <i class="fa fa-star-o fa-2x mdl-color-text--yellow"></i> Confirm End Of Service
                  </a>
                @endif
              @endforeach
            @endif
              @include('common.appointment_errors')
              @if(empty($data['data']['Appointments']))
                <i class="fa fa-user-times fa-5x"></i><br>
                <h4>You have no upcoming appointments yet.</h4>
              @endif
        @if(!empty($data['data']['Appointments']))
          @foreach($data['data']['Appointments'] as $appointment)
            @if($appointment['Appointment']['cancelled']!=true)
              <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                <div class="demo-card-event mdl-card mdl-shadow--2dp">
                  <div class="mdl-card__title mdl-card--expand">
                    <div class="bold">
                      <p class="line">{{$appointment['Service']['name']}}</p><br>
                      <small class="line color--mid-grey">Professional: {{$appointment['Supplier']['first_name']}}</small><br>                    
                      <small class="line mdl-color-text--grey">
                        <i class="fa fa-map-marker mdl-color-text--black"></i>
                        {{$appointment['MeetupAddress']['address_details']}}</small>
                    </div>                  
                    <div class="mdl-layout-spacer"></div>
                    <div id="timer" class="cancel">
                      <img class="inherit" id="{{$appointment['Appointment']['id']}}"
                              data-id="{{$appointment['Appointment']['id']}}" 
                              data-start_at="{{$appointment['Appointment']['start_at']}}" 
                             data-time="{{$appointment['Appointment']['supplier_start_at'] ? $appointment['Appointment']['supplier_start_at'] : 'null'}}"
                              src="{{URL::asset('icon/time-48.png')}}"><br>
                      <small class="bold" data-livestamp="{{date_format(date_create($appointment['Appointment']['start_at']),'U')}}"></small>
                    </div>
                  </div>
                  <div class="mdl-card__actions mdl-card--border no_padding">
                    <div class="central">
                        <a href="{{url('/appointments/chat')}}/{{$appointment['Appointment']['id']}}" class="left text_center border_round one-third">
                          <img class="thumbnails" src="{{URL::asset('icon/chat-48.png')}}">
                        </a>
                        <span @if(!$appointment['Appointment']['rescheduled']) onclick="reschedule(this.id)" @endif id="{{$appointment['Appointment']['id']}}" class="text_center one-third cancel">
                          <img class="thumbnails" id="{{$appointment['Appointment']['id']}}" src="{{URL::asset('icon/calendar-48.png')}}">
                        </span>
                        <span onclick="cancel_appointment(this.id)" id="{{$appointment['Appointment']['id']}}" class="right text_center border_round one-third cancel">
                          <img  class="thumbnails" id="{{$appointment['Appointment']['id']}}" src="{{URL::asset('icon/cancel-48.png')}}">
                        </span>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          @endforeach
        @endif

          </div>

        </section>

        <section class="mdl-layout__tab-panel" id="scroll-tab-2"> <!-- tab 2 -->
          
          <div class="mdl-grid">
            
            <!-- Past Appointment -->
            @if(empty($data['past']['data']['Appointments']))
                <i class="fa fa-user-times fa-5x"></i><br>
                <h4>You have no previous appointments.</h4>
              @endif
            @if(!empty($data['past']['data']['Appointments']))
              @foreach($data['past']['data']['Appointments'] as $past)
                @if($past['Appointment']['customer_id']==session('customer_id'))
                  <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                    <div class="demo-card-event mdl-card mdl-shadow--2dp">
                      <div class="mdl-card__title mdl-card--expand">
                        <div class="bold">
                          <p class="line">{{$past['Service']['name']}}</p><br>
                            <small class="line color--mid-grey">Professional: {{$past['Supplier']['first_name']}}</small><br>                    
                            <small class="line mdl-color-text--grey">
                              <i class="fa fa-map-marker mdl-color-text--black"></i>
                              {{$past['MeetupAddress']['address_details']}}</small>
                        </div>                  
                        <div class="mdl-layout-spacer"></div>
                        <div>
                          <img class="inherit" src="{{URL::asset('icon/done.png')}}"><br>
                          <span class="bold">{{date_format(date_create($past['Appointment']['start_at']),"dS-M-Y")}}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach
            @endif

          </div>

        </section>
      </main>
      <!-- /Page Content -->
    @endsection
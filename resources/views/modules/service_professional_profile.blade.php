       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            @if(isset($data) && !empty($data))
              <div class="mdl-card mdl-shadow--2dp about">
                <div class="mdl-grid">
                    <!-- Basic Info & Pic -->
                  <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <div class="mdl-card mdl-shadow--8dp">                  
                      <div class="profile_pic">
                        @if(is_null($data['professional_details']['pic_url']))
                          <img src="{{URL::asset('img/user.jpg')}}" alt="" />
                          @else<img src="{{$data['professional_details']['pic_url']}}" alt="" />
                        @endif
                      </div>
                      <h4>{{$data['professional_details']['first_name']}}</h4>
                      <span>
                        <i class="fa fa-star-o fa-2x mdl-color-text--pink"></i>   
                        <i class="fa fa-star-o fa-2x mdl-color-text--pink"></i>   
                        <i class="fa fa-star-o fa-2x mdl-color-text--pink"></i>   
                        <i class="fa fa-star-o fa-2x mdl-color-text--pink"></i>   
                        <i class="fa fa-star-o fa-2x mdl-color-text--pink"></i>                    
                      </span>          
                      <span>
                        {{$data['professional_details']['description']? $data['professional_details']['description']: ' '}}
                      </span>          
                    </div>
                  </div>
                    <!-- /Basic Info & Pic -->
                    <!-- Services offered -->
                  <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <div class="mdl-card mdl-shadow--8dp">                    
                      <h4>Services Offered</h4>         
                      <div class="drawer-separator"></div>
                      <div class="clr"></div>                                                 
                        <div class="title middle text_left bold">{{$data['category_details']['name']}}</div>
                        <div class="mdl-grid middle bold border-top border-left border-right">
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">Type:</div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">{{$data['service_details']['name']}}</div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">Price:</div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            @if($data['professional_details']['premium'])
                              Exclusive
                              @else
                              ‎₦{{number_format($data['service_details']['price']/100)}}
                            @endif
                            </div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">Time:</div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">{{$data['service_details']['appointment_time']}}</div>
                        </div>

                        <a href="<?php echo url('/services/location'); ?>" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Request Service   <i class="fa fa-arrow-right"></i></a>
                      </div>
                    </div>
                  </div>
                  <!-- /Services Offered -->
                </div>
              </div>
            @endif
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
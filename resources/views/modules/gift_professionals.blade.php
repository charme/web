       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">
                @if(empty($available_professionals))
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                  <a href="#">
                    <img src="{{URL::asset('img/user.jpg')}}" alt="" />
                    <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                      <span class="bold">No available Professionals</span>
                    </span>                  
                  </a>
                </div>

                @else
                  @foreach($available_professionals as $professionals)
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                    <a href="{{url('gift/selected-pro/profile')}}/{{$professionals['id']}}">
                      <img src="{{$professionals['pic_url'] ? $professionals['pic_url'] : URL::asset('img/user.jpg')}}" alt="" />
                      <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                        <span class="bold">{{$professionals['first_name']}}</span>
                      </span>                  
                    </a>
                  </div>
                  @endforeach
                @endif
              </div>
              

            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
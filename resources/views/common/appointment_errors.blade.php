          					  @if (session('error'))
                        <div class="bold mdl-color--red mdl-color-text--white text_center middle border-radius--5px padding--10px">
                         ERROR: {{ session('error') }}
                        </div>
                      @endif

                      @if (session('status'))
                        <div class="bold mdl-color--green mdl-color-text--white text_center middle border-radius--5px padding--5px">
                          {{ session('status') }}
                        </div>
                      @endif
                            <span class="mdl-color-text--white mdl-color--pink padding--5px bold central">
                              <span id="hide_services" class="cancel line" onclick="parent.$('#services_select').fadeToggle('slow');">
                                <i  class="fa fa-times-circle mdl-color-text--white"></i> 
                              </span>
                              <p class="line">Tick Services you offer</p>
                            </span>  
                            <div>      
                            <fieldset>                       
                            @foreach($data['services'] as $service) 

                              <label class="bold block border_round padding--5px cancel" for="{{$service['id']}}">
                                <input type="checkbox" id="{{$service['id']}}"
                              @foreach($data['supplier_services'] as $supplier_services)
                                @if($service['id'] == $supplier_services['service_id'])
                                  checked 
                                @endif
                              @endforeach
                                >
                                {{$service['name']}}
                              </label>
                            @endforeach
                            <br>
                            <span id="demo" class="mdl-color-text--pink padding--5px bold central">
                            </span>                              
                            <span id="save_services" onclick="SaveCat()" class="border-radius--5px mdl-color-text--white mdl-color--pink padding--5px bold central cancel">
                            Add Services
                            </span>  
                            </div>
<!--                        
                              <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect bold border_round padding--5px" for="checkbox-1">
                                <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                                <span class="mdl-checkbox__label text_left line">Checkbox</span>
                              </label> -->
                              <script type="text/javascript">
                                $('input:checkbox').simpleCheckbox();
                                
                                function close(){
                                  parent.$('#services_select').fadeToggle('slow');
                                }/*
                                $('#hide_services').click(function(){
                                  parent.$("#services_select").fadeToggle('slow');
                                });*/

                                function SaveCat() {
                                  var val = [];
                                  var id = [];
                                  $(':checkbox:checked').each(function(i){
                                  var url = null;
                                  var url = '{{url("/register/save_services/")}}';
                                   url += '/'+$(this).attr('id');                                  
                                  $.get(url);

                                       var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();

                                  });
                                  var un_val =[];
                                  var un_id=[];
                                  $(":checkbox:not(:checked)").each(function(i){
                                    var url = null;
                                  var url = '{{url("/register/delete_services/")}}';
                                  url += '/'+$(this).attr('id');
                                    var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();
                                  });                                    
                                }
                              </script>